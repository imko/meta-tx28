# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-imko a meta layer for the Yocto Project
# Embedded Linux build platform.
#
# meta-tx28 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# meta-tx28 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with meta-tx28. If not, see <http://www.gnu.org/licenses/>.

DESCRIPTION = "The Ka-Ro TX28 Linux kernel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"
DEPENDS += "lzop-native"

inherit kernel

PV = "3.3+git${SRCPV}"
PR = "r1"

# Tip of the karo-tx28 branch as of Thu Jan 31 15:50:44 CET 2013
SRCREV = "11c75c282f6c85d6e55dad29cd6ca4855f697922"

SRC_URI = "git://git.kernelconcepts.de/karo-tx-linux.git;branch=karo-tx28;protocol=git \
           file://defconfig"

S = "${WORKDIR}/git"

# We need to pass it as param since kernel might support more then one
# machine, with different entry points
EXTRA_OEMAKE += "LOADADDR=${UBOOT_ENTRYPOINT}"

COMPATIBLE_MACHINE = "tx28"
