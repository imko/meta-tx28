# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-imko a meta layer for the Yocto Project
# Embedded Linux build platform.
#
# meta-tx28 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# meta-tx28 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with meta-tx28. If not, see <http://www.gnu.org/licenses/>.

FILESEXTRAPATHS_prepend_tx28 := "${THISDIR}/${PN}:"

PR := "${PR}.1"

COMPATIBLE_MACHINE_tx28 = "tx28"
KBRANCH_tx28  = "standard/arm-versatile-926ejs"

SRC_URI += "file://tx28-standard.scc \
            file://tx28-user-config.cfg \
            file://tx28-user-patches.scc \
            file://tx28-user-features.scc \
           "
